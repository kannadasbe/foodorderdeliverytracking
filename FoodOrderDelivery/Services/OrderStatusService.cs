﻿using FoodOrderDelivery.Data.Interfaces;
using FoodOrderDelivery.DTOs;
using FoodOrderDelivery.Enums;
using FoodOrderDelivery.Models;
using FoodOrderDelivery.Services.Interfaces;
using FoodOrderDelivery.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodOrderDelivery.Services
{
    public class OrderStatusService : IOrderStatusService
    {
        private readonly IUnitOfWork _unitOfWork;
        public OrderStatusService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public void UpdateOrderStatus(int orderId, int orderStatus)
        {
            // Update Code here..
            if (orderStatus == (int)OrderStatusEnum.ReceivedByRestaurant)
            {
                var order = _unitOfWork.OrderRepository.GetByID(orderId);
                var deliveryPerson = GetDeliveryPersonForOrder(orderId, order.RestaurantId);
                UpdateDeliveryPerson(orderId, deliveryPerson.DeliveryPersonId);
            }
        }
        public Order GetOrderStatus(int orderId)
        {
            return new Order();
        }

        public OrderLiveStatus GetOrderLiveStatus(int orderId)
        {
            return new OrderLiveStatus();
        }

        public void UpdateDeliveryPersonLocation(DeliveryPerson deliveryPerson)
        {
            // Update Code here..
        }

        public void UpdateDeliveryPerson(int orderId,int deliveryPersonId)
        {
            // Update Code here..
        }


        /// <summary>
        /// Get nearest delivery person from the restaurant
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <param name="restaurantId"> Restaurant Id</param>
        /// <returns></returns>
        private DeliveryPerson GetDeliveryPersonForOrder(int orderId,int restaurantId)
        {
            var restaurant = GetRestaurantForOrder(restaurantId);
            var deliveryPeople = _unitOfWork.DeliveryPersonRepository.Get(null, null, "").ToList();
            List<DeliveryPersonLocation> deliverPersonLocation = new List<DeliveryPersonLocation>();
            foreach (var person in deliveryPeople)
            {
                var distance = new Coordinates(restaurant.Latitude, restaurant.Longitude)
                .DistanceTo(
                    new Coordinates(48.237867, 16.389477),
                    UnitOfLength.Kilometers
                );
                deliverPersonLocation.Add(new DeliveryPersonLocation()
                {
                    DeliveryPersonId = person.DeliveryPersonId,
                    Distance = distance
                });
            }
            //Return the nearest delivery person from the restaurant
            return deliverPersonLocation.OrderBy(x => x.Distance).FirstOrDefault();
        }

        //for demo purpose I have added all code in the same service file. But it should be moved to specific service file.
        private Restaurant GetRestaurantForOrder(int restaurantId)
        {
            //Code here..
            return new Restaurant();
        }
    }
}