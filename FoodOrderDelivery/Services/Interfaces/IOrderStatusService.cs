﻿using FoodOrderDelivery.DTOs;
using FoodOrderDelivery.Models;

namespace FoodOrderDelivery.Services.Interfaces
{
    public interface IOrderStatusService
    {
        OrderLiveStatus GetOrderLiveStatus(int orderId);
        Order GetOrderStatus(int orderId);
        void UpdateDeliveryPersonLocation(DeliveryPerson deliveryPerson);
        void UpdateOrderStatus(int orderId, int orderStatus);
    }
}