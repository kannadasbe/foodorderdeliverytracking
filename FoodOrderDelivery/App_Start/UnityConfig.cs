using FoodOrderDelivery.Data;
using FoodOrderDelivery.Data.Interfaces;
using FoodOrderDelivery.Services;
using FoodOrderDelivery.Services.Interfaces;
using System.Web.Http;
using Unity;
using Unity.Injection;
using Unity.WebApi;

namespace FoodOrderDelivery
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            container.RegisterType<FoodOrderContext>();

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IOrderStatusService, OrderStatusService>();
            
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}