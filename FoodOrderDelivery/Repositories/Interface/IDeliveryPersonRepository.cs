﻿using FoodOrderDelivery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FoodOrderDelivery.ViewModels;

namespace FoodOrderDelivery.Repositories.Interface
{
    /// <summary>
    /// Task resource repository
    /// </summary>
    public interface IDeliveryPersonRepository : IGenericRepository<Order>
    {

    }
}