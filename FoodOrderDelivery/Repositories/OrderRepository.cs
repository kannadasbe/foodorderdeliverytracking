﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FoodOrderDelivery.ViewModels;
using FoodOrderDelivery.Models;
using FoodOrderDelivery.Constants;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.SqlClient;
using FoodOrderDelivery.Data;
using FoodOrderDelivery.Repositories.Interface;

namespace FoodOrderDelivery.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(FoodOrderContext context)
            : base(context)
        { }

    }
}