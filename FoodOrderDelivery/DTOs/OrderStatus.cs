﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodOrderDelivery.DTOs
{
    public class OrderStatus
    {
        public int OrderId { get; set; }
        public int OrderStatusId { get; set; }
    }
    public class OrderLiveStatus : OrderStatus
    {
        public int DeliveryPersonId { get; set; }
        public double CurrentLatitude { get; set; }
        public double CurrentLongitude { get; set; }
    }
}