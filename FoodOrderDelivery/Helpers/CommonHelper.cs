﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace FoodOrderDelivery.Helpers
{
    /// <summary>
    /// Common helper methods
    /// </summary>
    public static class CommonHelper
    {
        private static NLog.Logger NLogger = NLog.LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Returns connection string from web config
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DBConnString"]?.ConnectionString;
        }

        /// <summary>
        /// Logs error message and exception
        /// </summary>
        /// <param name="source"></param>
        /// <param name="exception"></param>
        public static void LogError(string source, Exception exception)
        {
            var errorLog = new NLog.LogEventInfo(NLog.LogLevel.Error, "", source + " " + exception.Message);
            NLogger.Log(errorLog);
        }
        /// <summary>
        /// Logs error message
        /// </summary>
        /// <param name="message"></param>
        private static void LogError(string message)
        {
            var infoLog = new NLog.LogEventInfo(NLog.LogLevel.Error, "", message);
            NLogger.Log(infoLog);
        }
    }
}