﻿using NLog;
using FoodOrderDelivery.DTOs;
using FoodOrderDelivery.Models;
using FoodOrderDelivery.Services;
using FoodOrderDelivery.Services.Interfaces;
using FoodOrderDelivery.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FoodOrderDelivery.Controllers
{
    public class OrderStatusController : ApiController
    {
        private readonly IOrderStatusService _orderStatusService;
        private ILogger _logger = LogManager.GetCurrentClassLogger();

        public OrderStatusController(IOrderStatusService orderStatusService)
        {
            this._orderStatusService = orderStatusService;
        }

        // PUT: api/UpdateDeliveryPersonLocation
        /// <summary>
        /// Update Delivery Person Location
        /// </summary>
        /// <param name="orderId">OrderId</param>
        /// <param name="orderStatus">Order Status Id</param>
        [Route("UpdateDeliveryPersonLocation")]
        [HttpPut]
        public void UpdateDeliveryPersonLocation(DeliveryPerson deliveryPerson)
        {
            _orderStatusService.UpdateDeliveryPersonLocation(deliveryPerson);
        }

        // PUT: api/UpdateOrderStatus/5
        /// <summary>
        /// Update order status
        /// </summary>
        /// <param name="orderId">OrderId</param>
        /// <param name="orderStatus">Order Status Id</param>
        [Route("UpdateOrderStatus/{orderId:int}/{orderStatus:int}")]
        [HttpPut]
        public void UpdateOrderStatus(int orderId, int orderStatus)
        {
            _orderStatusService.UpdateOrderStatus(orderId, orderStatus);
        }


        /// <summary>
        /// Get Order Status By OrderId
        /// </summary>
        /// <param name="orderId"></param>
        [Route("GetOrderStatus/{orderId:int}")]
        [HttpGet]
        public IHttpActionResult GetOrderStatus(int orderId)
        {
            var responseWrapper = new ResponseWrapper<Order>();
            _logger.Info("Entering OrderStatusController - GetOrderStatus. OrderId - " + orderId.ToString());
            try
            {
                var response = _orderStatusService.GetOrderStatus(orderId);
                responseWrapper.Status = "Success";
                responseWrapper.Response = response;
            }
            catch(Exception ex)
            {
                //Log error and through relevant exception. 
                _logger.Error(ex, "Error occured in OrderStatus Controller Get Action");
                responseWrapper.Status = "Error";
                //Return relevant error message
                responseWrapper.Message = "Something went wrong..";
            }
            return Ok(responseWrapper);
        }

        /// <summary>
        /// Get Order Live Status 
        /// </summary>
        /// <param name="orderId">Order Id</param>
        [Route("GetOrderLiveStatus/{orderId:int}")]
        [HttpGet]
        public IHttpActionResult GetOrderLiveStatus(int orderId)
        {
            var responseWrapper = new ResponseWrapper<OrderLiveStatus>();
            _logger.Info("Entering OrderStatusController - GetOrderLiveStatus. OrderId - " + orderId.ToString());
            try
            {
                var response = _orderStatusService.GetOrderLiveStatus(orderId);
            }
            catch (Exception ex)
            {
                //Log error and through relevant exception. 
                _logger.Error(ex, "Error occured in OrderStatus Controller GetOrderLiveStatus Action");
                responseWrapper.Status = "Error";
                //Return relevant error message
                responseWrapper.Message = "Something went wrong..";
            }
            return Ok(responseWrapper);
        }
    }
}
