CREATE TABLE dbo.ResourceMaster
(
ResourceId INT IDENTITY PRIMARY KEY,
ResourceName VARCHAR(500)
)
GO

INSERT INTO dbo.ResourceMaster(ResourceName) VALUES('Kannadasan')
INSERT INTO dbo.ResourceMaster(ResourceName) VALUES('Manju')
INSERT INTO dbo.ResourceMaster(ResourceName) VALUES('Ranadhir')
GO

CREATE TABLE dbo.SkillMaster
(
SkillId INT IDENTITY PRIMARY KEY,
SkillName VARCHAR(500)
)
GO

INSERT INTO dbo.SkillMaster(SkillName) VALUES('Asp.Net MVC')
INSERT INTO dbo.SkillMaster(SkillName) VALUES('C#.Net')
INSERT INTO dbo.SkillMaster(SkillName) VALUES('MS Sql')
INSERT INTO dbo.SkillMaster(SkillName) VALUES('jQuery')
INSERT INTO dbo.SkillMaster(SkillName) VALUES('Angular')
GO

CREATE TABLE dbo.ResourceSkills
(
ResourceSkillId INT IDENTITY,
ResourceId INT FOREIGN KEY REFERENCES dbo.ResourceMaster(ResourceId),
SkillId INT FOREIGN KEY REFERENCES dbo.SkillMaster(SkillId)
CONSTRAINT  PK_ResourceSkills_ResourceId_SkillId PRIMARY KEY(ResourceId,SkillId)
)
GO

INSERT INTO dbo.ResourceSkills(ResourceId,SkillId) VALUES(1,1)
INSERT INTO dbo.ResourceSkills(ResourceId,SkillId) VALUES(1,2)
INSERT INTO dbo.ResourceSkills(ResourceId,SkillId) VALUES(1,3)
INSERT INTO dbo.ResourceSkills(ResourceId,SkillId) VALUES(2,2)
INSERT INTO dbo.ResourceSkills(ResourceId,SkillId) VALUES(2,3)
INSERT INTO dbo.ResourceSkills(ResourceId,SkillId) VALUES(2,4)
INSERT INTO dbo.ResourceSkills(ResourceId,SkillId) VALUES(3,3)
INSERT INTO dbo.ResourceSkills(ResourceId,SkillId) VALUES(3,4)
INSERT INTO dbo.ResourceSkills(ResourceId,SkillId) VALUES(3,5)
GO

CREATE TABLE dbo.ProjectMaster
(
ProjectId INT IDENTITY PRIMARY KEY,
ProjectName VARCHAR(500)
)
GO

INSERT INTO dbo.ProjectMaster(ProjectName) VALUES('Project A')
INSERT INTO dbo.ProjectMaster(ProjectName) VALUES('Project B')
GO



CREATE TABLE dbo.ProjectTasks
(
TaskId INT IDENTITY PRIMARY KEY,
TaskName VARCHAR(500),
ProjectId INT FOREIGN KEY REFERENCES dbo.ProjectMaster(ProjectId),
StartDate DATE,
EndDate DATE,
ResourceId INT FOREIGN KEY REFERENCES dbo.ResourceMaster(ResourceId)
)
GO

INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate,ResourceId) VALUES('Project A Task 1',1,'2021-12-14','2021-12-15',1)
INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate,ResourceId) VALUES('Project A Task 2',1,'2021-12-18','2021-12-19',1)
INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate,ResourceId) VALUES('Project A Task 3',1,'2021-12-20','2021-12-21',1)

INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate,ResourceId) VALUES('Project A Task 4',1,'2021-12-14','2021-12-15',2)
INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate,ResourceId) VALUES('Project A Task 5',1,'2021-12-16','2021-12-17',2)
INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate,ResourceId) VALUES('Project A Task 6',1,'2021-12-20','2021-12-21',2)

INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate,ResourceId) VALUES('Project A Task 7',1,'2021-12-14','2021-12-15',3)
INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate,ResourceId) VALUES('Project A Task 8',1,'2021-12-16','2021-12-17',3)
INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate,ResourceId) VALUES('Project A Task 9',1,'2021-12-18','2021-12-19',3)


INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate) VALUES('Project B Task 1',2,'2021-12-16','2021-12-17')
INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate) VALUES('Project B Task 2',2,'2021-12-18','2021-12-19')
INSERT INTO dbo.ProjectTasks(TaskName,ProjectId,StartDate,EndDate) VALUES('Project B Task 3',2,'2021-12-20','2021-12-21')



CREATE TABLE dbo.TaskSkills
(
TaskSkillId INT IDENTITY,
TaskId INT FOREIGN KEY REFERENCES dbo.ProjectTasks(TaskId),
SkillId INT FOREIGN KEY REFERENCES dbo.SkillMaster(SkillId)
CONSTRAINT PK_TaskSkills_TaskId_SkillId PRIMARY KEY(TaskId,SkillId)
)
GO

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(1,1)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(1,2)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(1,3)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(2,1)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(2,2)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(2,3)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(3,1)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(3,2)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(3,3)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(4,2)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(4,3)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(4,4)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(5,2)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(5,3)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(5,4)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(6,2)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(6,3)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(6,4)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(7,3)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(7,4)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(7,5)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(8,3)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(8,4)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(8,5)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(9,3)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(9,4)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(9,5)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(10,2)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(10,3)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(11,3)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(11,4)

INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(12,4)
INSERT INTO dbo.TaskSkills(TaskId,SkillId) VALUES(12,5)
GO

--dbo.GetProjectTasksWithResources 2
CREATE PROC dbo.GetProjectTasksWithResources 
@ProjectId INT
AS
BEGIN
SET NOCOUNT ON
	-- Declare required variables
	DECLARE @Tasks TABLE(TaskId INT,ResourceIds VARCHAR(500))
	DECLARE @StartDate DATE,@EndDate DATE,@SkillsCount INT,@TaskId INT,@TaskCount INT,@CurrentIndex INT=0
	DECLARE @Resources TABLE(ResourceId INT)
	DECLARE @ResourceList TABLE(ResourceId INT)
	DECLARE @ResourceIds VARCHAR(500)

	-- Declare cursor 
	DECLARE db_cursor CURSOR FOR 
	SELECT TaskId FROM dbo.ProjectTasks(NOLOCK) WHERE ProjectId=@ProjectId
	OPEN db_cursor FETCH NEXT FROM db_cursor INTO @TaskId  
	WHILE @@FETCH_STATUS = 0  
	BEGIN
		SET @ResourceIds=null
		DELETE FROM @Resources
		--Read stardate, enddate and skills count
		SELECT @StartDate=StartDate,@EndDate=EndDate FROM dbo.ProjectTasks(NOLOCK) WHERE TaskId=@TaskId
		SELECT @SkillsCount=COUNT(1) FROM dbo.TaskSkills(NOLOCK) WHERE TaskId=@TaskId

		-- Select skills available in the selected period and having 
		-- all required skills and insert it into @Resource table
		INSERT INTO @Resources(ResourceId)
		SELECT ResourceId FROM dbo.ResourceSkills(NOLOCK) WHERE SkillId IN(SELECT SkillId FROM dbo.TaskSkills(NOLOCK) WHERE TaskId=@TaskId)
		AND ResourceId NOT IN(SELECT ResourceId FROM dbo.ProjectTasks WHERE ResourceId IS NOT NULL AND StartDate<@EndDate AND EndDate>@StartDate)
		GROUP BY ResourceId HAVING COUNT(ResourceId)>=@SkillsCount

		SELECT @ResourceIds = COALESCE(@ResourceIds + ',','') + CAST(ResourceId as VARCHAR) FROM @Resources
		-- Insert the taskid and matching resourceids separated by comma in @Tasks table viable
		INSERT INTO @Tasks(TaskId,ResourceIds) VALUES(@TaskId,@ResourceIds)
		INSERT INTO @ResourceList(ResourceId)
		SELECT ResourceId FROM @Resources

		--Get the next record
		FETCH NEXT FROM db_cursor INTO @TaskId 
	END
	CLOSE db_cursor  
	DEALLOCATE db_cursor 
	
	--Result set
	SELECT a.TaskId,b.ResourceIds,a.TaskName,a.StartDate,a.EndDate FROM dbo.ProjectTasks(NOLOCK) a INNER JOIN @Tasks b on a.TaskId=b.TaskId
	SELECT DISTINCT a.ResourceId,a.ResourceName FROM dbo.ResourceMaster(NOLOCK) a INNER JOIN @ResourceList b ON a.ResourceId=b.ResourceId

SET NOCOUNT OFF
END
GO



