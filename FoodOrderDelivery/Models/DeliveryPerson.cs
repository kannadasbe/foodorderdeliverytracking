﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodOrderDelivery.Models
{
    public class DeliveryPerson
    {
        public int DeliveryPersonId { get; set; }
        public double CurrentLatitude { get; set; }
        public double CurrentLongitude { get; set; }
    }
    public class DeliveryPersonLocation : DeliveryPerson
    {
        public double Distance { get; set; }
    }
}