﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodOrderDelivery.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public int RestaurantId { get; set; }
        public byte StatusId { get; set; }
        public int DeliveryPersonId { get; set; }
    }
}