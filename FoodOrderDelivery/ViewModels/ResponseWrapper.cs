﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodOrderDelivery.ViewModels
{
    public class ResponseWrapper<T> where T : class
    {
        public string Status { get; set; }
        public T Response { get; set; }
        public string Message { get; set; }
    }
}