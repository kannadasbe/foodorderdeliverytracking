﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodOrderDelivery.Enums
{
    /// <summary>
    /// Order Status Enum
    /// </summary>
    public enum OrderStatusEnum
    {
        ReceivedByRestaurant = 1,
        FoodPrepared = 2,
        PickedupForDelivery = 3
    }
}