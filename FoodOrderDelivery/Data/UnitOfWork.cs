﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using FoodOrderDelivery.Data.Interfaces;
using FoodOrderDelivery.Models;
using FoodOrderDelivery.Repositories;
using FoodOrderDelivery.Repositories.Interface;

namespace FoodOrderDelivery.Data
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private readonly FoodOrderContext _context;

        public UnitOfWork(FoodOrderContext context)
        {
            this._context = context;
        }
        public UnitOfWork()
        {
            this._context = new FoodOrderContext();
        }
       

        private OrderRepository _orderRepository;

        public IOrderRepository OrderRepository => _orderRepository = _orderRepository ?? new OrderRepository(_context);

        private DeliveryPersonRepository _deliveryPersonRepository;

        public IDeliveryPersonRepository DeliveryPersonRepository => _deliveryPersonRepository = _deliveryPersonRepository ?? new DeliveryPersonRepository(_context);


        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}