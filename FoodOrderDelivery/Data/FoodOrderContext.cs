namespace FoodOrderDelivery.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using FoodOrderDelivery.Models;

    public partial class FoodOrderContext : DbContext
    {
        public FoodOrderContext()
            : base("name=DBConnString")
        {
        }

      
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<DeliveryPerson> DeliveryPersons { get; set; }
        public virtual DbSet<Restaurant> Restaurants { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           
        }
    }
}
