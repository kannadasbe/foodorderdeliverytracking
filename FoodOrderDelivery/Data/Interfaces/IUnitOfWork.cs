﻿using FoodOrderDelivery.Repositories.Interface;

namespace FoodOrderDelivery.Data.Interfaces
{
    public interface IUnitOfWork
    {
        IOrderRepository OrderRepository { get; }
        IDeliveryPersonRepository DeliveryPersonRepository { get; }

        void Dispose();
        void Save();
    }
}